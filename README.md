#Streaming Authoriser

#Foreword
I'm a member of the on-call team so I actively look after the majority of our legacy nodejs applications. 
I had started writing this in Kotlin (As this is what I have been writing for the best part of a year) and implemented a more complete architecture but upon reading the brief a couple more times, I've decided simple is best in this case.

#Brief
Build a service in Node.js that exposes an API which can be consumed from any
client. This service must check how many video streams a given user is watching
and prevent a user watching more than 3 video streams concurrently.

#Assumptions
I have assumed:

- This is a read only service, very lightweight, one specific role.
- There will be various other micro services that deal with account authorisation and stream sessions.
- We are looking to be cost efficient.
- Potential to move into multiple Availability Zones.


#HLD (Plus Potential Complete Architecture)
- Architecture Built; API Gateway -> AWS Lambda (Session Authoriser) -> DynamoDB.
- Conceptual; API Gateway -> AWS Lambda (Session Updater) -> DynamoDB. Authorisation Via Cognito.

![HLD](https://i.imgur.com/7PTG9Rd.png)
#AWS Development Information

Below you will find the endpoint for the service deployed in AWS:
```
https://ddu8q4cm17.execute-api.eu-west-1.amazonaws.com/development/streamauthoriser/request (GET)
```

I have populated the DynamoDB Table with two users for testing purposes. These are below:

- username: ryan (2 Active Streams)
- username: dazn (3 Active Streams)

Below you will find an example curl command, which will demonstrate a rejection on the dazn user.
Fail
```sh
curl -X GET \
  https://ddu8q4cm17.execute-api.eu-west-1.amazonaws.com/development/streamauthoriser/request \
  -H 'cache-control: no-cache' \
  -H 'username: dazn'
```
Success
```sh
curl -X GET \
  https://ddu8q4cm17.execute-api.eu-west-1.amazonaws.com/development/streamauthoriser/request \
  -H 'cache-control: no-cache' \
  -H 'username: ryan'
```
Unknown user
```sh
curl -X GET \
  https://ddu8q4cm17.execute-api.eu-west-1.amazonaws.com/development/streamauthoriser/request \
  -H 'cache-control: no-cache' \
  -H 'username: craig'
```

#Scaling Strategy
After the discussion with Rich and listening to the challenge for the company, I thought it would be good to tie in.
I have chosen AWS to deploy this micro service to for a number of good reasons:

API Gateway:

- Horizontally scalable across multi Availability Zones.
- API versioning to ensure compatibility between multiple API implementations that require zero code changes.
- Should you choose to move any downstream systems on prem, it's possible to reverse proxy/load balance across multiple servers.
- Mocking capability.
- Data caching mechanisms to improve API response times and performance.
- Offers API token generation.
- Price. It's roughly £3 for a million requests.

Interesting related read: https://aws.amazon.com/blogs/compute/building-a-multi-region-serverless-application-with-amazon-api-gateway-and-aws-lambda/

AWS Lambda:

- Automatic scaling.
- Can speak to other lambdas.
- Great language support. Potential to port older systems.
- Lambda Versioning.
- Plugs into cloudwatch for logs/metrics pooling.
- Pay as you go, rather than payment per hour.

DynamoDB:

- DAX. Caching mechanism.
- No more patching, security compliant. https://aws.amazon.com/compliance/services-in-scope/
- Option to provision capacity or use the recently released On-Demand. (In my experience, much better than the capacity limit/autoscaling solution)
- Atomic Transactions should you need them.
- Cross region replication. 

#Roadmap
- Find a better way to mock DynamoDB within unit tests.
- Investigate local/build pipeline integration testing strategy.
- Finish partially completed Terraform. Unfortunately ran out of time.