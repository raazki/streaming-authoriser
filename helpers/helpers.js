const buildResponse = (status, body) => ({
    statusCode: status,
    body: JSON.stringify({
        message: body
    })
});

const dbParams = (tableName, username) => ({
    TableName: tableName,
    Key: {
        "username": username
    }
});

module.exports = {
    buildResponse: buildResponse,
    dbParams: dbParams
};