const AWS = require('aws-sdk');
const helper = require('./helpers/helpers');

module.exports.handler = async (events) => {
    const {httpMethod, resource, headers} = events;
    const {username} = headers;

    //TODO https://github.com/dwyl/aws-sdk-mock#how-usage
    AWS.config.update({region: 'eu-west-1'});
    const dClient = new AWS.DynamoDB.DocumentClient();

    if (username === undefined) return helper.buildResponse(400, "Rejected - Missing username header.");

    if (resource !== '/streamauthoriser/request') return helper.buildResponse(404, "Resource not Found");

    if (httpMethod !== 'GET') {
        return helper.buildResponse(405, `Unsupported method: ${httpMethod}. Only GET Allowed.`);
    }

    const sessions = await getSessions(helper.dbParams("stage-session-authoriser", username), dClient);

    if (Object.keys(sessions).length === 0 && sessions.constructor === Object) {
        return helper.buildResponse(400, "Rejected - Unknown User.");
    }

    if (sessions.Item.sessions >= 3) {

        return helper.buildResponse(401, "Rejected - Too many sessions.");
    }
    //Should be returning a 418 here ;)
    return helper.buildResponse(200, "Authorised");
};

const getSessions = async (params, dClient) => dClient.get(params).promise();