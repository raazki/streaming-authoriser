terraform {
  backend "s3" {
    bucket = "session-authorisation-lambda-s3-state"
    key    = "state"
    region = "eu-west-1"
  }
}