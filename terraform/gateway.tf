resource "aws_api_gateway_rest_api" "sessionAuthoriser" {
  name = "SessionAuthoriserAPI"
  description = "An API to ensure that no given user can watch more than 3 streams concurrently"
}

resource "aws_api_gateway_deployment" "sessionAuthoriser" {
  rest_api_id = "${aws_api_gateway_rest_api.sessionAuthoriser.id}"
  stage_name = "Development"
}

resource "aws_api_gateway_resource" "sessionAuthoriser" {
  rest_api_id = "${aws_api_gateway_rest_api.sessionAuthoriser.id}"
  parent_id   = "${aws_api_gateway_rest_api.sessionAuthoriser.root_resource_id}"
  path_part   = "sessionAuthoriser"
}

resource "aws_api_gateway_method" "sessionAuthoriser" {
  rest_api_id   = "${aws_api_gateway_rest_api.sessionAuthoriser.id}"
  resource_id   = "${aws_api_gateway_resource.sessionAuthoriser.id}"
  http_method   = "ANY"
  authorization    = "NONE"
  api_key_required = "false"
}

resource "aws_api_gateway_integration" "get_fixture" {
  rest_api_id = "${aws_api_gateway_rest_api.sessionAuthoriser.id}"
  resource_id = "${aws_api_gateway_resource.sessionAuthoriser.id}"
  http_method = "${aws_api_gateway_method.sessionAuthoriser.http_method}"
  type = "AWS_PROXY"
  integration_http_method = "GET"
  uri = "${aws_lambda_function.sessionAuthoriser.invoke_arn}"
  credentials = "${aws_iam_role.api_gateway_lambda_exec.arn}"
}

resource "aws_iam_role" "api_gateway_lambda_exec" {
  name = "api_gateway_session_authoriser"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "sts:AssumeRole"
      ],
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}