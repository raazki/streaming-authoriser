data "archive_file" "lambda_zip" {
  type        = "zip"
  source_dir  = "lambda/sessionAuthoriserLambda"
  output_path = "sessionAuthoriserLambda.zip"
}

resource "aws_lambda_function" "sessionAuthoriser" {
  function_name = "sessionAuthoriser"
  source_code_hash = "${data.archive_file.lambda_zip.output_base64sha256}"

  s3_bucket = "${aws_s3_bucket.lBucket.id}"
  s3_key    = "${aws_s3_bucket_object.object.key}"

  handler = "handler"
  runtime = "nodejs8.10"

  role = "${aws_iam_role.lambda_exec.arn}"
}

resource "aws_iam_role" "lambda_exec" {
  name = "lambda_session_authorisation_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "sts:AssumeRole"
      ],
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_permission" "lambda_permission" {
  statement_id  = "AllowSessionAuthoriserToInvoke"
  action        = "lambda:InvokeFunction"
  function_name = "sessionAuthoriser"
  principal     = "apigateway.amazonaws.com"
}