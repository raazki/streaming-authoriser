resource "aws_s3_bucket" "lBucket" {
  bucket = "sessionAuthoriser-lambda-s3-state"
  acl    = "private"

  tags {
    Name        = "Session Authoriser and S3 State"
    Environment = "Development"
  }
}

resource "aws_s3_bucket_object" "object" {
  bucket = "${aws_s3_bucket.lBucket.id}"
  key    = "sessionAuthoriser"
  source = "${data.archive_file.lambda_zip.output_path}"
  etag   = "${md5(file("${data.archive_file.lambda_zip.output_path}"))}"
}