const assert = require('assert');
const set = require('lodash/set');
const AWS = require('aws-sdk-mock');

const index = require('../index');

context('Streaming Authoriser', () => {

    let event;

    beforeEach(() => {
        event = {
            "resource": "/",
            "path": "/",
            "httpMethod": "GET",
            "headers": {
                "Accept": "*/*",
                "accept-encoding": "gzip, deflate",
                "cache-control": "no-cache",
                "Host": "ddu8q4cm17.execute-api.eu-west-1.amazonaws.com",
                "Postman-Token": "26e4ba30-3df5-4d3f-afcc-41e5ddd1a0c5",
                "User-Agent": "PostmanRuntime/7.6.0",
                "username": "",
                "X-Amzn-Trace-Id": "Root=1-5c533847-6d0f2822640c62fb3d1fcdfe",
                "X-Forwarded-For": "5.151.68.29",
                "X-Forwarded-Port": "443",
                "X-Forwarded-Proto": "https"
            },
            "pathParameters": null,
            "stageVariables": null,
            "requestContext": {
                "resourceId": "j9u3becw5g",
                "resourcePath": "/",
                "httpMethod": "GET",
                "extendedRequestId": "UYW7HFIiDoEFxxQ=",
                "requestTime": "31/Jan/2019:18:02:47 +0000",
                "path": "/development",
                "accountId": "252240171704",
                "protocol": "HTTP/1.1",
                "stage": "development",
                "domainPrefix": "ddu8q4cm17",
                "requestTimeEpoch": 1548957767185,
                "requestId": "6a5b3890-2582-11e9-aeac-f376157b1c92",
                "identity": {
                    "cognitoIdentityPoolId": null,
                    "accountId": null,
                    "cognitoIdentityId": null,
                    "caller": null,
                    "sourceIp": "0.0.0.0",
                    "accessKey": null,
                    "cognitoAuthenticationType": null,
                    "cognitoAuthenticationProvider": null,
                    "userArn": null,
                    "userAgent": "PostmanRuntime/7.6.0",
                    "user": null
                },
                "domainName": "ddu8q4cm17.execute-api.eu-west-1.amazonaws.com",
                "apiId": "ddu8q4cm17"
            },
            "body": null,
            "isBase64Encoded": false
        };
    });

    afterEach(() => {
        AWS.restore('DynamoDB.DocumentClient', 'get');
    });

    it('should return 400 when request is received that doesn\'t have a username header', async () => {
        delete event.headers.username;
        const response = await index.handler(event);
        assert.equal(response.statusCode, 400);
    });

    it('should return 405 when request is received that isn\'t GET with Request Resource', async () => {
        const setUsername = set(event, 'headers.username', 'dazn');
        const setMethod = set(setUsername, 'httpMethod', 'POST');
        const request = set(setMethod, 'resource', '/streamauthoriser/request');
        const response = await index.handler(request);
        assert.equal(response.statusCode, 405);
    });

    it('should return 405 when request is received that isn\'t GET with Request Resource', async () => {
        const setMethod = set(event, 'httpMethod', 'POST');
        const request = set(setMethod, 'resource', '/streamauthoriser/request');
        const response = await index.handler(request);
        assert.equal(response.statusCode, 405);
    });

    it('should return 200 when a user with less than 3 active streams requests authorisation', async () => {
        const setMethod = set(event, 'httpMethod', 'GET');
        const request = set(setMethod, 'resource', '/streamauthoriser/request');

        AWS.mock('DynamoDB.DocumentClient', 'get', Promise.resolve({Item: {sessions: 2}}));

        const response = await index.handler(request);
        assert.equal(response.statusCode, 200);
    });

    it('should return 400 when a user with more than 3 active streams requests authorisation', async () => {
        const setMethod = set(event, 'httpMethod', 'GET');
        const request = set(setMethod, 'resource', '/streamauthoriser/request');

        AWS.mock('DynamoDB.DocumentClient', 'get', Promise.resolve({Item: {sessions: 3}}));

        const response = await index.handler(request);
        assert.equal(response.statusCode, 401);
    });

    it('should return 400 when a request is received from an unknown user', async () => {
        const setMethod = set(event, 'httpMethod', 'GET');
        const request = set(setMethod, 'resource', '/streamauthoriser/request');

        AWS.mock('DynamoDB.DocumentClient', 'get', Promise.resolve({}));

        const response = await index.handler(request);
        assert.equal(response.statusCode, 400);
    });

    it('should return 404 when there is no recognised resource path', async () => {
        const response = await index.handler(event);
        assert.equal(response.statusCode, 404);
    });

});